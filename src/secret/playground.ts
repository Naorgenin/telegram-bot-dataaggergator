var a: string = "Ḩeşār-e Sefīd";
var b: string = "rammatgan";

function create_matrix(m: string, n: string) {
    var result = [];
    for (var _ of m) {
        result.push(new Array(n.length).fill(0));
    }
    return result;
}
var matrix: number[][] = create_matrix(a, b);

function print_matrix(mat: number[][]) {
    for (let i = 0; i < mat.length; i++) {
        console.log(...mat[i]);
    }
}
console.log(matrix.length, matrix[0].length);
// popluate all obvious deletions\insertions
for (var i = 0; i < matrix[0].length; i++) {
    matrix[0][i] = i + 1;
}
for (var j = 0; j < matrix.length; j++) {
    matrix[j][0] = j + 1;
}
print_matrix(matrix);
console.log("#################");
for (let i = 1; i < matrix.length; i++) {
    for (var j = 1; j < matrix[0].length; j++) {
        var subcost: number;
        if (a[i] == b[j]) {
            subcost = 0;
        } else {
            subcost = 1;
        }
        matrix[i][j] = Math.min(
            matrix[i - 1][j] + 1, // deletion
            matrix[i][j - 1] + 1, // insertion
            matrix[i - 1][j - 1] + subcost //substitution
        );
    }
}
print_matrix(matrix);
console.log("distance:" + matrix[matrix.length - 1][matrix[0].length - 1]);
