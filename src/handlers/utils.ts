export function remove_white_space(some_string: string) {
    return some_string.replace(/\s+/, " ");
}

export function grab_input(some_string: string[]) {
    return some_string.slice(1).join(" ");
}

export function get_city_name(some_string: Array<string>) {
    return some_string.slice(1).join(" ");
}
