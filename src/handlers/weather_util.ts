export function create_matrix(m: string, n: string) {
    var result = [];
    for (var _ of m) {
        result.push(new Array(n.length).fill(0));
    }
    return result;
}

export function print_matrix(mat: number[][]) {
    for (let i = 0; i < mat.length; i++) {
        console.log(...mat[i]);
    }
}

export function levenshtein_distance(
    cityA: string,
    cityB: string,
    matrix: number[][]
) {
    // Initialize first row and column with appropriate values (base cases)
    for (var i = 0; i < matrix[0].length; i++) {
        matrix[0][i] = i; 
    }
    for (var j = 0; j < matrix.length; j++) {
        matrix[j][0] = j; 
    }

    // Fill the matrix by comparing characters of cityA and cityB
    for (let i = 1; i < matrix.length; i++) {
        for (var j = 1; j < matrix[0].length; j++) {
            var subcost: number;
            
            // Remember: `i` and `j` are 1-based indices in the matrix but 0-based in strings
            if (cityA[i - 1] == cityB[j - 1]) {  
                subcost = 0;
            } else {
                subcost = 1;
            }
            
            // Compute the minimum cost for each operation
            matrix[i][j] = Math.min(
                matrix[i - 1][j] + 1,      // deletion
                matrix[i][j - 1] + 1,      // insertion
                matrix[i - 1][j - 1] + subcost // substitution
            );
        }
    }

    return matrix[matrix.length - 1][matrix[0].length - 1];
}