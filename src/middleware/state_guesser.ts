export function guessedState(ctx, next) {
    if (ctx.updateType === "callback_query") {
        const text = ctx.callbackQuery.message.text;
        if (text.startsWith("Did you mean")) {
            const match = text.match(/(?<=mean\s)(.*?)(?=\?)/);
            let guess;
            if (match !== null) {
                if (match[1]) {
                    guess = match[1];
                }
            }
            ctx.state.command = {
                raw: text,
                guess,
            };
        }
    }
    return next();
}
