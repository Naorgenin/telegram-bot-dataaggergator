import axios, { AxiosResponse } from "axios";

export class Corona {
    // 151= israel
    static url: string =
        "https://covid-tracker-us.herokuapp.com/v2/locations/151?source=jhu&timelines=true";
    constructor() {}

    // this is the correct approch to async calls
    // https://github.com/microsoft/TypeScript/issues/15292#issuecomment-372844204
    async process_data(): Promise<string> {
        try {
            const response = await axios.get(Corona.url);
            const { last_updated, latest } = response.data.location;

            const formattedDate = new Date(last_updated).toLocaleString();
            const confirmedCases = latest.confirmed;
            const confirmedDeaths = latest.deaths;

            return `Israel's latest Covid-19 Stats as of : ${formattedDate} 
Confirmed cases:  ${confirmedCases}
Confirmed deaths: ${confirmedDeaths}`;
        } catch (err) {
            return `failed retriving covid-data..\n ${err}`;
        }
    }
}

//TODO check if graphs are possible, same api, using the timeline feature!
