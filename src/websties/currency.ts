import axios from "axios";
import { Icurrecyjson } from "../interfaces/currency_response";

export class ConvertCash {
    constructor(public authorization: string) {
        this.authorization = authorization;
    }
    // an ever expanding list of possible alternative symbols that arent in the api
    static alternative_names = { NIS: "ILS" };
    // prettier-ignore
    static current_symbols = ["AED","AFN","ALL","AMD","ANG","AOA","ARS","AUD","AWG","AZN","BAM","BBD","BDT","BGN","BHD","BIF","BMD","BND","BOB","BRL","BSD","BTC","BTN","BWP","BYN","BYR","BZD","CAD","CDF","CHF","CLF","CLP","CNY","COP","CRC","CUC","CUP","CVE","CZK","DJF","DKK","DOP","DZD","EGP","ERN","ETB","EUR","FJD","FKP","GBP","GEL","GGP","GHS","GIP","GMD","GNF","GTQ","GYD","HKD","HNL","HRK","HTG","HUF","IDR","ILS","IMP","INR","IQD","IRR","ISK","JEP","JMD","JOD","JPY","KES","KGS","KHR","KMF","KPW","KRW","KWD","KYD","KZT","LAK","LBP","LKR","LRD","LSL","LTL","LVL","LYD","MAD","MDL","MGA","MKD","MMK","MNT","MOP","MRO","MUR","MVR","MWK","MXN","MYR","MZN","NAD","NGN","NIO","NOK","NPR","NZD","OMR","PAB","PEN","PGK","PHP","PKR","PLN","PYG","QAR","RON","RSD","RUB","RWF","SAR","SBD","SCR","SDG","SEK","SGD","SHP","SLL","SOS","SRD","STD","SVC","SYP","SZL","THB","TJS","TMT","TND","TOP","TRY","TTD","TWD","TZS","UAH","UGX","USD","UYU","UZS","VEF","VND","VUV","WST","XAF","XAG","XAU","XCD","XDR","XOF","XPF","YER","ZAR","ZMK","ZMW","ZWL",];

    // update names in the list for the api
    swap_alternative_names(list_input: string[]) {
        let copied_list = list_input;
        for (const [index, element] of list_input.entries()) {
            if (element in ConvertCash.alternative_names) {
                copied_list[index] = ConvertCash.alternative_names[element];
            }
        }
        return copied_list;
    }
    async convert_currency(from: string, to: string, amount: number = 1) {
        try {
            var url = `https://api.apilayer.com/exchangerates_data/convert?to=${to}&from=${from}&amount=${amount}`;
            const response = await axios.get(url, {
                headers: {
                    apikey: this.authorization,
                },
            });
            const { success, result } = response.data;

            if (success == true) {
                return `${amount} ${from} equals ${result} ${to}`;
            } else {
                return `failed accessing https://api.apilayer.com/exchangerates_data/`;
            }
        } catch (error) {
            return `An error occurred while accessing the currency conversion API: ${error.message}`;
        }
    }

    sanatize_input(param_list: string[]) {
        // Find and remove 'to' if it exists in param_list
        const indexOfTo: number | undefined = param_list.indexOf("to");
        if (indexOfTo !== -1) {
            param_list.splice(indexOfTo, 1);
        }
        // swap alt names
        let readylist = this.swap_alternative_names(param_list);

        for (const symbol of readylist) {
            if (!ConvertCash.current_symbols.includes(symbol)) {
                throw `Unkown symbol ${symbol}`;
            }
        }
        return readylist;
    }
}
