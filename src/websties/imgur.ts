var axios = require("axios");

export class Imgur {
    static url: string =
        "https://api.imgur.com/3/gallery/hot/day/1?mature=true&album_previews=false"; //consider removing from constructor into fucntion
    constructor(public authorization) {
        this.authorization = authorization;
    }

    async getImages(limit = "3"): Promise<object> {
        try {
            const response = await axios.get(Imgur.url, {
                headers: {
                    Authorization: this.authorization,
                },
            });
            const data_to_process = response.data.data
                .filter((post: any) => post.link) // Filter out posts without a link
                .slice(0, parseInt(limit)) // Limit the number of results
                .map((post: any) => ({
                    title: post.title,
                    url: post.link,
                }));

            return data_to_process;
        } catch (error) {}
    }
}
