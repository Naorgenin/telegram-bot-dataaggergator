import axios from "axios";

export class Reddit {
    constructor(
        public id: string,
        public secret: string,
        public user: string,
        public password: string
    ) {
        this.id = id;
        this.secret = secret;
        this.user = user;
        this.password = password;
    }
    // prettier-ignore
    async get_subreddit(subreddit: String, limit: string = "3"): Promise<object | string> {
        subreddit = subreddit.toLowerCase();
        try {
            const response = await axios.get(
                `https://www.reddit.com/r/${subreddit}.json?limit=${parseInt(limit)}`,
                {
                    headers: {
                        id: this.id,
                        secret: this.secret,
                    },
                }
            );
            const data_to_process = response.data.data.children.map((post: any) => ({
                title: post.data.title,
                url: post.data.url,
            }));
            return data_to_process;
        } catch (error) {
            return `Sorry , i couldnt access r/${subreddit}, for weebs:${error}`;
        }
    }
}
