import axios from "axios";

import * as list_of_cities from "../resources/city_list.json";
import { create_matrix, levenshtein_distance } from "../handlers/weather_util";
import { Icityjson } from "../interfaces/weather-interface";

export class Weather {
    constructor(public api_key: string) {
        this.api_key = api_key;
    }
    async Convert_City_Name_To_Id(target_city: string) {
        var guessed_city: Icityjson = { id: -1, name: "" };
        if (target_city == "") {
            return guessed_city;
        }
        for (const cityjson of Object.values(<Icityjson[]>list_of_cities)) {
            if (target_city.toLowerCase() == cityjson["name"].toLowerCase())
                return cityjson;
        }
        return null;
    }
    /**
     * return our best guess for the city
     * make a Levenshtein distance matrix
     * return d[m,n] lowest score is our guess
     * @param target_city
     * @returns Icityjson
     */
    async Guess_City_Name_To_Id(target_city: string) {
        var guessed_city: Icityjson = { id: -1, name: "" };
        if (target_city == "") {
            return guessed_city;
        }
        var minDistance = Number.MAX_SAFE_INTEGER;
        let count: number = 0;
        Object.values(list_of_cities).forEach((city: Icityjson) => {
            let distance: number = levenshtein_distance(
                target_city,
                city.name,
                create_matrix(target_city, city.name)
            );
            if (distance <= minDistance) {
                minDistance = distance;
                guessed_city = city;
                count++;
                // console.log("guess: " + city.name + " distance:" + distance);
            }
        });
        // console.log("Total guesses: " + count);
        return guessed_city;
    }

    async call_api(city: Icityjson): Promise<string> {
        try {
            const response = await axios.get(
                `https://api.openweathermap.org/data/2.5/weather?id=${city.id}&appid=${this.api_key}&units=metric`
            );
            const data = response.data;

            const timestamp = data.dt * 1000;
            const tempature = data.main.temp;
            const time = new Date(timestamp).toLocaleString();

            return `Weather in ${city.name}: ${tempature} C \nrecorded at ${time}`;
        } catch (error) {
            console.log(error);
            return `Failed accessing api.openweathermap.org. error:${error}`;
        }
    }
}
