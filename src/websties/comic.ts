import axios from "axios";

export async function getImage(): Promise<string> {
    try {
        const res = await axios.get("https://c.xkcd.com/random/comic/");
        const returned_string: String = res.data;
        // FROM HTML -> "https://imgs.xkcd.com/comics/thinking_ahead.png
        const possible_image: RegExpMatchArray = returned_string.match(
            /https:\/\/imgs[^'"]+\.png/
        );
        if (!possible_image) {
            return "Failed to find the comic image URL";
        }
        return possible_image[0];
    } catch (error) {
        console.log(`Reason: ${error}`);
        return `Failed to fetch the comic. error:${error}`;
    }
}
