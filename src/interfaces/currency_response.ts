export interface Icurrecyjson {
    date: string;
    result: number;
    success: boolean;
}
