import { Context } from "telegraf";

export interface Data {
    id: number;
    name: string;
}

export interface BotContext extends Context {
    session?: Data;
}
