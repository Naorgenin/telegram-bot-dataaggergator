console.time("Boot");
import "dotenv/config";
import { ROUTE_UNDER_CONSTRUCTION } from "./resources/consts";
import { Context, Telegraf, session } from "telegraf";
import { BotContext, Data } from "./interfaces/custom_bot";
import { Reddit } from "./websties/reddit";
import { Weather } from "./websties/weather";
import { Imgur } from "./websties/imgur";
import { Corona } from "./websties/covid19";
import { getImage } from "./websties/comic";
// import { greet } from "./handlers/greet";
import { remove_white_space, get_city_name } from "./handlers/utils";
import { Icityjson } from "./interfaces/weather-interface";
import { ConvertCash } from "./websties/currency";
// middleware
import { guessedState } from "./middleware/state_guesser";
// // Todo
// //1. remove CITY JSON LIST from files, it should be stored at a database

let reddit_obj = new Reddit(
    process.env.ID,
    process.env.SECRET,
    process.env.USER,
    process.env.PASSWORD
);
let imgur_obj: Imgur = new Imgur(process.env.IMGURAUTH);
let weather_obj: Weather = new Weather(process.env.WEATHER_APIKEY);
let covid_obj: Corona = new Corona();
let currency_obj = new ConvertCash(process.env.CURRENCY_APIKEY);

const bot: Telegraf<BotContext> = new Telegraf(process.env.API_TOKEN);

bot.use(guessedState);
bot.use(session({ defaultSession: () => ({ id: 0, name: -1 }) }));
bot.catch((err, ctx: Context) => {
    console.log(`Ooops, encountered an error for ${ctx.updateType}`, err);
});

bot.start((ctx) => ctx.reply("Welcome"));
// bot.help((ctx) => ctx.reply("Send me a sticker"));
bot.on("sticker", (ctx) => ctx.reply("👍"));
bot.command("hipster", Telegraf.reply("λ"));
bot.command("oldschool", (ctx) => ctx.reply("Hello"));
bot.help((ctx) =>
    ctx.reply(`Lost, aren't we?
No worries, here is a summary of what i can do:
Commands:
make the robot say hello
/greet

grab headlines from reddit
/reddit <subreddit> [NumberofPosts]
ex: /reddit funny 5

get weather data
/weather <city>
ex: /weather Beersheba
    /weather haifa

samples from a imgur collection
/imgur [NumberofPosts]
ex : /imgur 5

get Currency rates:
/convert <amount?> <From Symbol> <to?> <To Symbol>
ex: 1 USD to NIS

A random xkcd comic :
/comic

Israels current covid stats:
/corona -> covid stats
`)
);
// TODO put the handle of greet here do not pass ctx
// bot.hears(/\b(sup|Sup|hello|Hello|hey|Hey|hi|Hi)/, (ctx) => {
//     greet(ctx);
// });
bot.command("greet", (ctx) => {
    var choices = [
        "Hey there",
        "Wadup",
        "Sup",
        "Shalom",
        "Yo",
        "Heyooo",
        "Hola",
        "Greetings",
        "Ne-hao",
        "Eat shit",
    ];
    console.log(`${ctx.from.first_name} used the Bot!`);
    ctx.reply(
        `${choices[Math.floor(Math.random() * choices.length)]} ${
            ctx.from.first_name
        }`
    );
});
bot.command("reddit", (ctx) => {
    var input = remove_white_space(ctx.message.text).split(" ");
    var subreddit = input[1];
    var limit = input[2];
    if (subreddit == null) {
        ctx.reply("I need a subreddit to look for, for example: \n /reddit funny 5");
    }
    var text = `Searching in /r${subreddit}`;
    ctx.reply(text);
    let result: Promise<object | string> = (async () => {
        return await reddit_obj.get_subreddit(subreddit, limit);
    })();
    result.then((res: object) => {
        // probably wrong type
        if (Object.keys(res).length == 0) {
            ctx.reply("Sorry couldnt find that Subreddit");
        }
        Object.values(res).forEach((element) => {
            if (res[0] != undefined) {
                ctx.reply(element["title"] + "\n" + element["url"]);
            }
        });
    });
});
bot.command("imgur", (ctx) => {
    var input = remove_white_space(ctx.message.text).split(" ");
    var limit = input[1];
    ctx.reply("opening imgur..");
    let result: Promise<object> = (async () => {
        return await imgur_obj.getImages(limit);
    })();
    result.then((res: object) => {
        // probably wrong type

        if (Object.keys(res).length == 0) {
            ctx.reply("Sorry couldnt get images from imgur :(");
        }
        Object.values(res).forEach((element) => {
            ctx.reply(element["title"] + "\n" + element["url"]);
        });
    });
});
// //make a link to all cities available to me
// // do not send ctx to get_weather
bot.command("weather", async (ctx) => {
    var input = remove_white_space(ctx.message.text).split(" ");
    var cityname = get_city_name(input);
    if (cityname == "" || cityname == null) {
        ctx.reply("Provide a city for me - an example: \n /weather Beersheba ");
    } else {
        const returned_city: Icityjson = await weather_obj.Convert_City_Name_To_Id(
            cityname
        );
        if (returned_city == null || returned_city.id == -1) {
            await ctx.telegram.sendMessage(
                ctx.chat.id,
                "your city isnt on the list, Let me make a guess"
            );
            const guessed_city: Icityjson = await weather_obj.Guess_City_Name_To_Id(
                cityname
            );
            const { id, name } = guessed_city;
            ctx.session.id = id;
            ctx.session.name = name;
            await ctx.telegram.sendMessage(
                ctx.chat.id,
                `Did you mean ${guessed_city.name}?`,
                {
                    reply_markup: {
                        inline_keyboard: [
                            [{ text: "Yes", callback_data: "guess_city" }],
                            [{ text: "No", callback_data: "not_found" }],
                        ],
                    },
                }
            );
        } else {
            var res = await weather_obj.call_api(returned_city);
            ctx.reply(res);
        }
    }
});

// // do not send ctx to  getImage
bot.command("comic", async (ctx) => {
    ctx.reply(await getImage());
});
bot.command("corona", (ctx) => {
    ctx.telegram.sendMessage(ctx.chat.id, "pick a function", {
        reply_markup: {
            inline_keyboard: [
                [{ text: "Covid stats", callback_data: "covid" }],
                [{ text: "Covid graph", callback_data: "covid_graph" }],
            ],
        },
    });
});
bot.action("covid", async (ctx) => {
    ctx.deleteMessage();
    ctx.reply(ROUTE_UNDER_CONSTRUCTION);
    // ctx.reply(await covid_obj.process_data());
});
bot.action("covid_graph", (ctx) => {
    ctx.deleteMessage();
    ctx.reply(ROUTE_UNDER_CONSTRUCTION);
});

// // /convert 1 usd to nis
// // TODO create sanatize input function that returns clean input -> else raise exception
bot.command("convert", async (ctx) => {
    try {
        var input = remove_white_space(ctx.message.text).split(" ");

        var params: string[] = input.slice(1); // remove command
        if (input.length < 2) {
            console.log("Not enough params - heres an example: 1 USD to NIS");
        }
        if (params.length > 3) {
            // if number exists
            try {
                var amount = parseInt(params[0]);
                var params = params.slice(1); // cut number
            } catch (error) {
                console.log(error);
                ctx.reply("Invalid Input: try -> /convert 1 USD to NIS");
                return;
            }
        }

        let readylist = currency_obj.sanatize_input(params);
        ctx.reply(
            await currency_obj.convert_currency(readylist[0], readylist[1], amount)
        );
    } catch (error) {
        console.log(error);
        ctx.reply("Failed Converting ");
    }
});

bot.action("guess_city", async (ctx) => {
    ctx.deleteMessage();
    const guessed_city: Icityjson = { id: ctx.session.id, name: ctx.session.name };
    ctx.reply(await weather_obj.call_api(guessed_city));
});
bot.action("not_found", (ctx) => {
    ctx.deleteMessage();
    ctx.reply(`Sorry couldn't find that city`);
});
// // regex exp /words/
// bot.hears(/fuck\s*you/, (cts) => {
//     cts.reply("No, fuck you.");
// });
console.log("Bot is on!");
bot.launch();

process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));
console.timeEnd("Boot");

// TODO:
// 1. try to turn json data into graph and post as a replaytsc
